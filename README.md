# nadezhkina
 
Реализованы два паттерна: абстракная фабрика (AbstractFactory) и стратегия (Strategy)

Суть:
Создаётся две фабрики - фабрика с созданием героя ближнего боя и с созданием героя дальнего боя

Создаётся класс битва, в который через конструктор поступает игрок, участвующий в битве и стратегия

Результат представлен на картинке ниже:
![alt text](Patterns_lab_2/Mics/Result.png)
