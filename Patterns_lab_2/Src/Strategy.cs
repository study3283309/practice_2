﻿using System.Threading.Channels;

namespace Patters_lab_2;

public interface IAction
{
    public void Action(Player player);
}

public class Defense : IAction
{
    public void Action(Player player)
    {
        Console.WriteLine("I'm defending myself");
        player.ShowComponents();
    }
}

public class Attack : IAction
{
    public void Action(Player player)
    {
        Console.WriteLine("I'm attacking");
        player.ShowComponents();
    }
}

public class Battle
{
    private readonly Player player;

    private readonly IAction action;

    public Battle(Player player, IAction action)
    {
        this.action = action;
        this.player = player;
    }

    public void Fight()
    {
        action.Action(player);
    }
}