﻿
namespace Patters_lab_2;

class Program
{
    static void Main(string[] args)
    {
        IPlayerFactory factoryMelee = new MeleeWarriorFactory();
        IPlayerFactory factoryRanged = new RangedWarriorFactory();

        var battle1 = new Battle(new Player(factoryMelee), new Defense());
        var battle2 = new Battle(new Player(factoryRanged), new Defense());
        var battle3 = new Battle(new Player(factoryRanged), new Attack());

        Console.WriteLine("battle1 -- the melee player defends himself");
        battle1.Fight();
        Console.WriteLine("battle2 -- the ranged player defends himself");
        battle2.Fight();
        Console.WriteLine("battle3 -- ranged player attacks");
        battle3.Fight();
        
        
        Console.Read();
    }
}