﻿namespace Patters_lab_2;

public interface IMovement
{
    public void ReleaseMove();
}

public interface IWeapon
{
    public void ReleaseWeapon();
}

public class FlyMovement : IMovement
{
    public void ReleaseMove()
    {
        Console.WriteLine("I can fly");
    }
}

public class RunMovement : IMovement
{
    public void ReleaseMove()
    {
        Console.WriteLine("I can run");
    }
}

public class Sword : IWeapon
{
    public void ReleaseWeapon()
    {
        Console.WriteLine("I can attack with a sword");
    }
}

public class Gun : IWeapon
{
    public void ReleaseWeapon()
    {
        Console.WriteLine("I can attack with a gun");
    }
}

public interface IPlayerFactory
{
    public IMovement CreateMovement();
    public IWeapon CreateWeapon();
}
public class RangedWarriorFactory : IPlayerFactory
{
    public IMovement CreateMovement() => new FlyMovement();

    public IWeapon CreateWeapon() => new Gun();
}

public class MeleeWarriorFactory : IPlayerFactory
{
    public IMovement CreateMovement() => new RunMovement();

    public IWeapon CreateWeapon() => new Sword();
}

public class Player
{
    private IMovement movement;
    private IWeapon weapon;

    public Player(IPlayerFactory factory)
    {
        movement = factory.CreateMovement();
        weapon = factory.CreateWeapon();
    }

    public void ShowComponents()
    {
        movement.ReleaseMove();
        weapon.ReleaseWeapon();
    }
}